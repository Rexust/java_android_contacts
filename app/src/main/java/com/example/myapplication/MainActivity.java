package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.myapplication.adapters.PeopleAdapter;
import com.example.myapplication.models.Person;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PeopleAdapter.OnPeopleAdapterListener {

    ListView lvPeople;
    Button btnCreate;
    Button btnSave;
    EditText etFirstName;
    EditText etLastName;
    EditText etPhone;
    EditText etAge;
    EditText etImagePath;
    PeopleAdapter adapter;
    Person editPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvPeople = findViewById(R.id.lvPeople);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etPhone = findViewById(R.id.etPhone);
        etAge = findViewById(R.id.etAge);
        etImagePath = findViewById(R.id.etImagePath);
        btnCreate = findViewById(R.id.btnCreate);
        btnSave = findViewById(R.id.btnSave);

        ArrayList<Person> people = new ArrayList<>();

        people.add(new Person("First Name 1", "Last Name 1", "+380996465321", 22,
                "https://avatanplus.ru/files/resources/original/57b89792ded2c156a91015e1.png"));
        people.add(new Person("First Name 2", "Last Name 2", "+3805098765412", 34,
                "https://www.pxpng.com/public/uploads/preview/-11601774644rkfopjcrfk.png"));
        people.add(new Person("First Name 3", "Last Name 3", "+3806625874167", 50,
                "https://e7.pngegg.com/pngimages/249/206/png-clipart-the-boss-baby-infant-the-bossier-baby-diaper-youtube-toddler-boss-child-head.png"));

        adapter = new PeopleAdapter(this, R.layout.person_layout, people);
        lvPeople.setAdapter(adapter);

        btnCreate.setOnClickListener(v -> {
            try {
                people.add(new Person(
                        etFirstName.getText().toString(),
                        etLastName.getText().toString(),
                        etPhone.getText().toString(),
                        Integer.parseInt(etAge.getText().toString()),
                        etImagePath.getText().toString()));
                adapter.notifyDataSetChanged();
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
            etFirstName.setText("");
            etLastName.setText("");
            etPhone.setText("");
            etAge.setText("");
            etImagePath.setText("");
            editPerson = null;
        });

        btnSave.setOnClickListener(v -> {
            try {
                editPerson.setFirstName(etFirstName.getText().toString());
                editPerson.setLastName(etLastName.getText().toString());
                editPerson.setPhone(etPhone.getText().toString());
                editPerson.setAge(Integer.parseInt(etAge.getText().toString()));
                editPerson.setImagePath(etImagePath.getText().toString());
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
//                e.printStackTrace();
            }
        });
    }

    @Override
    public void onEditClick(Person person) {
        etFirstName.setText(person.getFirstName());
        etLastName.setText(person.getLastName());
        etPhone.setText(person.getPhone());
        etAge.setText(Integer.toString(person.getAge()));
        etImagePath.setText(person.getImagePath());
        editPerson = person;
    }

}